
// DEPENDENCIES
	const User = require("../models/User.js");
	const bcrypt = require("bcrypt"); // for hashing the password
	const auth = require("../auth.js"); // for authentication


// ----------------------------------------------------------------------------------------------------

// CHECKING IF USER'S EMAIL ALREADY EXISTS IN THE DATABASE
/*
	Business Logic: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

	module.exports.checkIfEmailExists = (requestBody) => {
		return User.find({email: requestBody.email}).then(result => {
			if(result.length > 0) {
				return true;
			}
			else{
				return false;
			};
		});
	};

// -------------------------------------------

// USER REGISTRATION
/*
	Business Logic:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

	module.exports.registerUser = (requestBody) => {
		let newUser = new User({
			firstName: requestBody.firstName,
			lastName: requestBody.lastName,
			email: requestBody.email,
			mobileNo: requestBody.mobileNo,
			password: bcrypt.hashSync(requestBody.password, 10)
			// protects the user's password by showing them as random string characters upon viewing
			// 10 is the (standard) value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password (in layman terms, its the number of layers of encryption)
		});

		return newUser.save().then((user, err) => {
			if(err){
				return false;
			}
			else{
				return true;
			};
		});
	};

// -------------------------------------------

// USER AUTHENTICATION
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

	module.exports.authenticateUser = (requestBody) => {
		return User.findOne({email: requestBody.email}).then(result => {
			if(result == null) {
				return false;
			}
			else {
				const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)
				if(isPasswordCorrect) {
					return {access: auth.createAccessToken(result)};
				}
				else{
					return false;
				};
			};
		});
	};

// ----------------------------------------------------------------------------------------------------

// ACTIVITY

	module.exports.getProfile = (requestBody) => {
		return User.findOne({_id: requestBody.id}).then(result => {
			if(result == null) {
				return false;
			}
			else {
				result.password = "";
				return result;
			};
		});
	};