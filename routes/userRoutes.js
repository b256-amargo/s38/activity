
// DEPENDENCIES
	const express = require("express");
	const router = express.Router();
	const userController = require("../controllers/userControllers.js");

// ----------------------------------------------------------------------------------------------------

// ROUTE FOR CHECKING IF USER'S EMAIL ALREADY EXISTS IN THE DATABASE
	router.post("/checkEmail", (request, response) => {
		userController.checkIfEmailExists(request.body).then(resultFromController => response.send(resultFromController));
	});

// ROUTE FOR USER REGISTRATION
	router.post("/register", (request, response) => {
		userController.registerUser(request.body).then(resultFromController => response.send(resultFromController));
	});

// ROUTE FOR USER AUTHENTICATION
	router.post("/login", (request, response) => {
		userController.authenticateUser(request.body).then(resultFromController => response.send(resultFromController));
	})

// ----------------------------------------------------------------------------------------------------

// ACTIVITY
	router.post("/details", (request, response) => {
		userController.getProfile(request.body).then(resultFromController => response.send(resultFromController));
	})

// ----------------------------------------------------------------------------------------------------

// EXPORT
	module.exports = router;