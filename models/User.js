
// DEPENDENCIES
	const mongoose = require("mongoose");

// SCHEMA
	const userSchema = new mongoose.Schema({
		firstName: {
			type: String,
			required: [true, "Please enter your first name (required)."]
		},

		lastName: {
			type: String,
			required: [true, "Please enter your last name (required)."]
		},

		email: {
			type: String,
			required: [true, "Please enter your email (required)."]
		},

		password: {
			type: String,
			required: [true, "Please enter your password (required)."]
		},

		isAdmin: {
			type: Boolean,
			default: false
		},

		mobileNo: {
			type: String,
			required: [true, "Please enter your mobile number (required)."]
		},

		enrollments: [
			{
				courseId: {
					type: String,
					required: [true, "Course ID required"]
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				},
				status: {
					type: String,
					default: "Enrolled"
				}
			}
		]
	});

// MODEL / EXPORT
	module.exports = mongoose.model("User", userSchema);